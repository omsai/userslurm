# Specify shorter munge socket path to workaround UNIX path length limitation:
# munged: Error: Exceeded maximum length of 108 bytes for socket pathname
CONFIGURE_OPTS := --prefix=$(PREFIX) \
--silent \
--with-munge-socket=/tmp/munge.socket.2
