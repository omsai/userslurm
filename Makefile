TARGETS := all clean
SUBDIRS := munge slurm

INSTALLDIR := install
PREFIX := $(PWD)/$(INSTALLDIR)
export INSTALLDIR
export PREFIX

MUNGED := $(INSTALLDIR)/sbin/munged
SLURMCTLD := $(INSTALLDIR)/sbin/slurmctld
SLURMD := $(INSTALLDIR)/sbin/slurmd

ZIP := $(notdir $(PWD)).zip
SOURCES := $(filter-out .gitignore,$(shell git ls-tree -r --name-only HEAD))

.PHONY : $(TARGETS) $(SUBDIRS)
all : $(SUBDIRS)
$(filter-out all,$(TARGETS)) : $(SUBDIRS)
$(SUBDIRS) :
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY : start
start :
	$(RM) $(INSTALLDIR)/var/log/*.log $(INSTALLDIR)/var/log/munge/*.log
	$(MUNGED)
	$(SLURMCTLD)
	$(SLURMD)

.PHONY : stop
stop :
	pkill slurmd
	pkill slurmctld
	pkill munged

clean :
	$(RM) -r $(INSTALLDIR)

.PHONY : zip
zip : $(ZIP)
$(ZIP) : $(SOURCES)
	zip $@ $^
