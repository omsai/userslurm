# Create single computer setup like SchedMD's "Introduction to SLURM, part 8"
# https://www.youtube.com/watch?v=VozfZGZIX8w

INSTALLDIR := ../$(INSTALLDIR)
URL := https://github.com/SchedMD/slurm/archive/slurm-20-02-2-1.tar.gz
TARBALL := $(notdir $(URL))
SRCDIR := slurm-$(TARBALL:.tar.gz=)
BUILDDIR := build-slurm
FILE_CONFIGURE_OPTS := configure.mk
include $(FILE_CONFIGURE_OPTS)
MAKEFILE := $(BUILDDIR)/Makefile
SBATCH := $(INSTALLDIR)/bin/sbatch
SLURM_CONF := $(INSTALLDIR)/etc/slurm.conf
HOSTNAME := $(shell hostname)
SLURMCTLD := $(INSTALLDIR)/sbin/slurmctld

.PHONY : all install
all : install
install : $(SBATCH) $(SLURM_CONF)

$(SLURM_CONF) : slurm.conf
	install -D $< $@

slurm.conf : slurm.conf.in
	sed -e 's#@HOSTNAME@#$(HOSTNAME)#' -e 's#@PREFIX@#$(PREFIX)#' -e 's#@USER@#$(USER)#' $< > $@

$(SBATCH) : $(MAKEFILE) | $(BUILDDIR)
	$(MAKE) -C $(BUILDDIR) -j 4 install

$(MAKEFILE) : $(SRCDIR)/configure $(FILE_CONFIGURE_OPTS) | $(BUILDDIR)
	cd $(BUILDDIR) && ../$< $(CONFIGURE_OPTS)

$(BUILDDIR) :
	mkdir -p $@

$(SRCDIR)/configure : $(TARBALL)
	tar -xf $<
	touch -r $< $@

$(TARBALL) :
	wget -O $@ $(URL)

.PHONY : clean
clean :
	$(RM) -r $(BUILDDIR) $(SRCDIR) slurm.conf
